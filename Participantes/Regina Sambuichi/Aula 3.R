#Aula três (aula 2 no slides)
#Instalação do pacote devtools
install.packages("devtools")
library(devtools)
#Instalando um pacote do github microdadosBrasil
help("install_github")
install_github ("lucasmation/microdadosBrasil")
install.packages("readr")
#Para ler o dicionário da PNAD
dic <- readr::read_csv("bancos/dicionario_pessoas.csv")
str(dic)  
#outra maneira dic <- readr::read_csv (file.choose())
#Exercício 2 aula três, lendo o arquivo AMOSTRA_DF_PNAD2009p.txt
help("fwf_widths")
widths <- readr::fwf_widths(widths = dic$tamanho2, col_names =dic$cod2) 
str(widths)
pnad09.df <- readr::read_fwf(file = "bancos/AMOSTRA_DF_PNAD2009p.txt", col_positions = widths)
#Exercício 3 Lendo a base com o read_PNad do microdadosBrasil
pnad_microBr <- microdadosBrasil::read_PNAD(ft = "pessoas", i= 2009, file = "bancos/AMOSTRA_DF_PNAD2009p.txt")
#Questão 1
install.packages("readxl")
readxl::excel_sheets("bancos/datasets.xls")
#Questão 2
chickwts.df <- readxl::read_excel(path = "bancos/datasets.xls", sheet=3)
##Questão 3
files <- "bancos/datasets.xls"
planilhas <- lapply(readxl::excel_sheets (files), 
                     function(x) readxl::read_excel (files, sheet =x))
str(planilhas)
#Para separar cada planilha
iris <- planilhas[[1]]
mtcars <- planilhas[[2]]
chickwts <- planilhas [[3]]
quakes<- planilhas [[4]]
#Para juntar todas as planilhas em uma só quando todas tiverem a mesmo número de colunas
for(i in 1:length(planilhas)){final <- rbind(final,planilhas[[1]])}
